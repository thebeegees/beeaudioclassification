import os
import matplotlib.pyplot as plt
from scipy import fft
from scipy.io import wavfile
import numpy, math, re
from python_speech_features import mfcc


class Input_File:
    def __init__(self, file, queen):
        '''Contains all information of a single input file. Each file will be split into smaller objects'''
        self.list_of_samples = []
        self.file = file
        self.Open_file()
        self.queen = queen


    def Open_file(self):
        '''Opens the wav file and turns stereo into mono audio'''

        self.sample_rate, self.sample = wavfile.read(self.file) #Opens the file and extracts 2 features

        self.sample_length = self.sample.shape[0]
        self.duration = self.sample_length / self.sample_rate #The duration is the amount of samples divided by the rate
        if self.sample.shape[1] == 2: #If the file contains 2 streams(stereo) take only one stream
            self.sample = self.sample.T[1]
            # self.sample = self.sample / 32767 #Normalize 16 bit wav bitrate


    def Split_File(self):
        '''If the file is larger than 2 times the target length it will split the file into smaller pieces
        each piece gets an associated object.'''

        Target_length = 3
        Target_sample_length = int(round(Target_length * self.sample_rate))
        start = int(round(0))
        stop = int(round(Target_sample_length))

        if self.duration > 2*Target_length:
            #if input file is longer than 2 times the target length it gets split up if it is shorter it wont split
            remainder = self.sample_length % Target_sample_length
            rounded = self.sample_length - remainder
            sample_amount = int(rounded / Target_sample_length)

            print(remainder,"Samples are cut away |" , rounded, "Samples will be used |", sample_amount, "Sample objects per file")

            for i in range(sample_amount):
                sample = Sample(self.sample[start:stop],self.sample_rate, i, self.file, self.queen, Target_length)
                start += Target_sample_length
                stop += Target_sample_length
                self.list_of_samples.append(sample)

        else:
            sample = Sample(self.sample ,self.sample_rate, "main", self.file, self.queen, Target_length)
            self.list_of_samples.append(sample)

        return Target_length


class Sample(Input_File):
    def __init__(self, sample, sample_rate, name, filename, queen, file_length):
        '''This object is a single sample from a audio file containing all properties.
        These objects will be used for processing.'''
        self.name = name
        self.sample = sample
        self.filename = filename
        self.sample_rate = sample_rate
        self.file_length = file_length

        self.Properties() #Run the properties function
        self.Abs_Fourier()  #Take the absolute values of the fft.
        self.mfcc_feat = mfcc(sample, sample_rate, nfft=len(sample), winlen=self.file_length, highfreq=800) #Generate mfcc coefficients for each sample.
        self.mfcc_length = len(self.mfcc_feat[0])
        self.queen = queen


    def Properties(self):
        '''This method gets all properties of the wav file'''
        self.sample_length = self.sample.shape[0]
        self.duration = self.sample_length / self.sample_rate
        self.sample_duration = 1 / self.sample_rate

        self.fft = fft(self.sample) #Runs the scipy fft function on the file

        index = self.filename.index("Hive")
        self.hive = self.filename[index:index + 5]

        date = re.findall(r"[\d]{1,2}_[\d]{1,2}_[\d]{4}", self.filename) #Used to find the date in the filename
        if len(date) > 0:
            date = date[0].replace("_", "-")
            self.date = date

        time = re.search(r"[\d]{2}_[\d]{2}_[\d]{2}[.]", self.filename) #Used to find the timestamp in the filename
        if time:
            time = time.group().replace("_", ":")
            self.time = time[:-1]

        return


    def Abs_Fourier(self):
        '''This method gets the absolute values from the fft.'''
        '''http://samcarcagno.altervista.org/blog/basic-sound-processing-python/?doing_wp_cron=1550581760.1320049762725830078125'''
        n = len(self.sample)
        p = self.fft
        nPoint = int(math.ceil((n+1)/2.0))
        p = p[0:nPoint]
        p = abs(p)
        p = p / float(n)
        p = p ** 2

        if n % 2 > 0:
            p[1:len(p)] = p[1:len(p)] * 2
        else:
            p[1:len(p) - 1] = p[1:len(p) - 1] * 2

        freqs = self.sample_rate / n
        self.freqArray = numpy.arange(0, nPoint, 1.0) * freqs
        self.p = p
        return

    def Basic_Plots(self ,path):
        '''This method generates 3 basic plots for every sample into one image '''
        fig, axes = plt.subplots(3, 1,constrained_layout=False)
        plt.tight_layout()

        #Amplitude plot of sample
        axes[0].plot(self.sample)
        axes[0].set_xlabel('Sample')
        axes[0].set_ylabel('Amplitude')
        axes[0].title.set_text('Amplitude plot')

        #Spectrogram plot
        self.Pxx, freqs, bins, im = axes[1].specgram(self.sample, Fs=self.sample_rate)
        axes[1].set_xlabel('Time')
        axes[1].set_ylabel('Frequency')
        axes[1].title.set_text('Spectrogram')

        # #Power Spectrum plot
        axes[2].plot(self.freqArray, 10 * (numpy.log10(self.p)), color='k')
        axes[2].set_xlabel('Frequency (Hz)')
        axes[2].set_ylabel('Power (dB)')
        axes[2].set_xlim(0,800)
        axes[2].title.set_text('Power Spectrum plot')

        #Generate new output folder for the plots
        path = path+os.sep+"Images"
        if not os.path.exists(path):
            os.makedirs(path)

        plt.show()
        #plt.savefig(path+os.sep+"{}_{}_{}_{}.png".format(self.name, self.hive, self.date,self.time), dpi=fig.dpi)
        plt.clf()
        plt.close()
