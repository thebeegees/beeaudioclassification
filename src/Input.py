import os, yaml

file_list = []

class Data:
    def __init__(self):
        pass
    def Import(self, path):
        for file in os.listdir(path):
            if file.endswith(".wav"):
                file_list.append(path+os.sep+file)
        return file_list

    def Open_config(self):
        try:
            with open("Config.yaml", 'r') as ymlfile: #If the file Config.yaml can not be found remove the ../ from this line.
                cfg = yaml.load(ymlfile)
        except FileNotFoundError:
            with open("../Config.yaml", 'r') as ymlfile: #If the file Config.yaml can not be found remove the ../ from this line.
                cfg = yaml.load(ymlfile)
        return cfg

