import sys
from src.Input import Data
from src.AudioProces import Input_File
from src.Analysis import Analysis

class Control:
    def __init__(self):
        return

    def proces_sample(self, file_list):
        '''This method will make a object from the input file and keep track of all made objects'''
        object_list = []
        for file in file_list: #For every file make an object with the information of that file
            if "NO" in file:
                queen = "NO"
            else:
                queen = "YES"
            object = Input_File(file, queen)
            self.length = Input_File.Split_File(object)
            object_list.append(object)
        return object_list

def main(args):
    path = args[1] #Only the path is given as an argument.
    file_list = Data.Import(Data,path) #Find all wav files in the given path and assign them to file_list
    frequency_bands = Data.Open_config(Data) #Import the config file containing frequency bands

    controller = Control
    all_objects = controller.proces_sample(controller, file_list) #Generate a object Input_File object in AudioProces and add the objects to lists
                                                                #Each object file will get split by the target length into smaller samples.

    tasks = Analysis(all_objects, path, frequency_bands, controller.length) #All function are in analysis and can be called individually here
    print("Files have been split")

    ### TASKS NEEDED TO RUN ###


    tasks.frequency_band_to_array()   #Used to extract the frequency range from the config file.
    df = tasks.make_dataframe()     #Always required for the pca function
    tasks.run_pca(df)               #Outputs a csv with the pca data

    # tasks.make_plots()                #Generates all the plots for every sample into the Images folder
    # tasks.mean_to_single_file()     #Used to generate the mean bins output as csv
    tasks.output_best_freq()        #Find the most variant frequencies and output results into output folder
    tasks.mfcc_to_csv()             #Outputs the extracte mfcc features.


if __name__ == '__main__':
    sys.exit(main(sys.argv))