from sklearn.preprocessing import StandardScaler

from src.MeansFreqBand import SpectogramAnalysis
import math
from sklearn.decomposition import PCA
import numpy
import pandas as pd
from itertools import chain
import os, itertools, csv
from scipy import stats


class Analysis:
    def __init__(self, all_objects, path, frequency_bands, length):
        self.all_samples = self.get_all_samples(all_objects)
        self.get_output_path(path)
        self.config = frequency_bands
        self.length = length
        self.band_index = []


    def get_all_samples(self, all_objects):
        '''Get all samples from every file into one list for processing.'''
        temp = [x.list_of_samples for x in all_objects]
        all_samples = list(chain.from_iterable(i for i in temp))
        return all_samples


    def get_output_path(self, path):
        '''Make a output directory if it doesnt exist'''
        output_path = path.split(os.sep)[0:]
        output_path.append("output_final")  #Enter the name of your output directory here.
        output_path = "/".join(output_path)
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        self.output_path = output_path

        return


    def make_plots(self):
        '''This method calls the plot function inside the sample for every file'''
        print("Plotting graphs for {} samples. This may take some time.".format(len(self.all_samples)))

        for sample in self.all_samples:
            sample.Basic_Plots(self.output_path)

        return


    def mean_to_single_file(self):
        '''Call the SpectrogramAnalysis class with all the samples to be processed into frequency bins.'''
        analyser = SpectogramAnalysis(self.all_samples, self.output_path)
        analyser.looper()

        return


    def frequency_band_to_array(self):
        '''Get the frequency band from a config file and cut them from the power spectrum
        after the data is aquired this function calls to make the data into a matrix'''
        bands = self.config['regions']

        for sample in self.all_samples:
            self.freq_array = sample.freqArray
            self.freq_column = []
            self.freq_row = []
            self.freq_index = []

            for band in bands: #For every frequency band in the config file.
                self.band = bands[band]
                start = int(math.floor(bands[band][0] * self.length))
                stop = int(math.trunc(bands[band][1] * self.length))
                step = self.freq_array[1]* self.length
                self.freq_column.append(self.freq_array[start:stop].tolist())
                index = numpy.arange(start, stop, step)
                self.freq_index.append(index.tolist())

            for freqs in self.freq_index:
                for freq in freqs:
                    freq = int(round(freq))
                    temp = 10 * numpy.log10(sample.p[freq])
                    self.freq_row.append(temp)
            sample.freq_amp = self.freq_row
        self.make_freq_list()

        return


    def make_freq_list(self):
        '''Turn the data from the frequency band into a single array to be used as column labels'''
        frequency_list = []
        for band in self.freq_column:
            for freq in band:
                frequency_list.append(freq)

        self.frequency_list = frequency_list

        return


    def run_pca(self, result):
        '''Run a PCA on the fft data for every sample in the sample list of the selected frequencies'''

        #Use only the data from the features in this case the frequencies
        features = self.frequency_list
        data = result.loc[:, features].values

        #Run this method to get the variation per frequency of the result dataframe
        self.freq_var(result,features)

        #Run the PCA using fit_transform to reduce the dimensions.
        print("Running pca")
        data = StandardScaler().fit_transform(data)
        pca = PCA(n_components=3)
        components = pca.fit_transform(data)
        dataframe = pd.DataFrame(data=components, columns=["Component 1", "Component 2", "Component 3"])

        #This method will use the loading frames from the pca and look for the min and max values
        #and using these values to find the corresponding frequencies this is used to validate the output of the
        #self.freq_var function.

        # self.pca_var(pca, features)

        #Add the labels to the new pca dataframe
        dataframe = pd.concat([dataframe, result["Queen"], result["Hive"], result["Date"], result["Time"]], axis=1)
        dataframe.to_csv(r"{}/amplitude pca.csv".format(self.output_path), index = None, header=True)

        return


    def mfcc_to_csv(self):
        '''Gets the mfcc spectrum from the object and output it as csv file'''
        dataframe = pd.DataFrame()

        for x in self.all_samples:
            feat = x.mfcc_feat
            mfcc = pd.DataFrame(data=feat, columns=[["mfcc {}".format(x) for x in range(0,x.mfcc_length)]])
            mfcc["Queen"] = x.queen
            mfcc["Hive"] = x.hive

            try:
                if len(x.date) > 0 :
                    mfcc["Date"] = x.date
            except AttributeError:
                mfcc["Date"] = None

            try:
                if len(x.time) > 0 :
                    mfcc["Time"] = x.time
            except AttributeError:
                mfcc["Time"] = None
            dataframe = dataframe.append(mfcc, ignore_index=True)

        dataframe.to_csv("{}/mfcc.csv".format(self.output_path, self.length), index=None, header=True)

        return


    def freq_var(self, result, features):
        '''This function calculates the variation of all the frequency columns and selects the 100 highest values
        The corresponding frequencies are grouped together for every 10 hz. '''
        v = result.var()
        v = v.tolist()
        v = [round(x, 5) for x in v]
        v_sort = sorted(v, reverse=True)
        vtop = v_sort[:100]
        vars = []
        for item in vtop:
            x = v.index(item)
            vars.append(x)

        vars2 = []
        for i in vars:
            vars2.append(features[i])

        vars2 = sorted(vars2)
        vars3 = []
        for k, g in itertools.groupby(vars2, key=lambda n: n // 10):
            k = k * 10
            x = list(g)
            t, p = self.run_t_test(result, x, features)
            vars3.append([k, len(x), t, p])

        self.freq_result = sorted(vars3, key=lambda x: x[1], reverse=True)


    def pca_var(self, pca, features):
        '''This function takes a pca object and calculates the 50 min and max values from the loadings
        and uses these values to find the corresponding frequency columns this is used to verify the results from
        from the freq_var method.'''
        loadings = pca.components_

        result_dict = {}
        p= 0
        for load in loadings:
            hi = []
            lo = []
            p += 1
            sort_load = sorted(load, reverse=True)
            max_list = sort_load[:100]
            min_list = sort_load[-100:]
            for item in max_list:
                x = numpy.where(load == item)
                hi.append(x[0][0])
            for item in min_list:
                x = numpy.where(load == item)
                lo.append(x[0][0])

            hi_freq = []
            lo_freq = []
            for i in hi:
                hi_freq.append(features[i])

            for i in lo:
                lo_freq.append(features[i])

            hi_freq = sorted(hi_freq)
            lo_freq = sorted(lo_freq)


            hi_freq_sig = []
            lo_freq_sig = []


            for k, g in itertools.groupby(hi_freq, key=lambda n: n // 10):
                k = k * 10
                x = list(g)
                hi_freq_sig.append([k, len(x)])

            for k, g in itertools.groupby(lo_freq, key=lambda n: n // 10):
                k = k * 10
                x = list(g)
                lo_freq_sig.append([k, len(x)])

            hi_result = sorted(hi_freq_sig, key=lambda x: x[1], reverse=True)
            lo_result = sorted(lo_freq_sig, key=lambda x: x[1], reverse=True)

            result_dict[p] = [lo_result, hi_result]
        self.result_dict = result_dict


    def output_best_freq(self):

        with open("{}/found_frequencies.csv".format(self.output_path), 'w') as output:
            writer = csv.writer(output)
            writer.writerows([["These are the results of the 100 most variant frequencies found in the variation of all data"]])
            writer.writerows([["Frequency range", "Percentage of most variable frequencies", "T-score", "P-value"]])

            for i, var in enumerate(self.freq_result):
                writer.writerows([["{}-{} Hz ".format(int(var[0]), (int(var[0]+10))), " {} %".format(var[1]),"{}".format(var[2]), "{}".format(var[3])]])

            # writer.writerows(["\n"])
            # for key, value in self.result_dict.items():
            #     writer.writerows([["These are the results of 100 most variant frequencies of component {}".format(key)]])
            #     writer.writerows([["Frequency range", "Percentage of most variable frequencies"]])
            #     for vars in self.result_dict[key]:
            #         for var in vars:
            #             writer.writerows([["{}-{} Hz ".format(int(var[0]), (int(var[0] + 10))), " {} %".format(var[1])]])
            #     writer.writerows(["\n"])

        os.system("echo 'Output has been generated'")
        output.close()


    def make_dataframe(self):
        result = pd.DataFrame()
        print("Generating Dataframe for all samples this could take a while")

        for sample in self.all_samples:  # For every sample add a row to the dataframe
            freq_amp = pd.DataFrame(data=[sample.freq_amp], columns=[x for x in self.frequency_list])

            # Add all labels from the sample object to the dataframe
            freq_amp["Queen"] = sample.queen
            freq_amp["Hive"] = sample.hive

            try:
                if len(sample.date) > 0:
                    freq_amp["Date"] = sample.date
            except AttributeError:
                freq_amp["Date"] = None

            try:
                if len(sample.time) > 0:
                    freq_amp["Time"] = sample.time
            except AttributeError:
                freq_amp["Time"] = None
            result = result.append(freq_amp, ignore_index=True)
        print('Dataframe is done')
        return result


    def run_t_test(self, result, index_freqs, features):

        n = result.loc[result['Queen'] == "NO"]
        y = result.loc[result['Queen'] == "YES"]

        n = n.mean(axis=0)
        y = y.mean(axis=0)

        n_list = n.values.tolist()
        y_list = y.values.tolist()

        input = sorted(index_freqs)

        item = []
        for x in input:
            y = features.index(x)
            item.append(y)
        a, b = stats.ttest_ind(y_list[item[0]:item[-1]], n_list[item[0]:item[-1]], equal_var=False)

        return a,b
