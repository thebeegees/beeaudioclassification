import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

class SpectogramAnalysis:

    def __init__(self, allsampels, output):
        '''

        :param allsampels: list of all the sliced audio samples
        :param output: output dir
        '''
        self.samplelist = allsampels
        self.meanMatrix = []
        self.queenlist = []
        self.output_path = output

    def spectrogramdeterminer(self, sample):
        '''
        calculates the spectrogram of a single sample and returns the PxX matrix

        :param sample: single sample from the self.samplelist
        :return: PxX, a matrix that contains a numeric representation of the spectrogram figure
        '''
        fig, axes = plt.subplots()
        PxX, freqs, bins, im = axes.specgram(sample.sample, NFFT=2048, Fs=sample.sample_rate, noverlap=20)
        del freqs, bins, im
        return PxX

    def meancalculator(self, Pxx):
        '''
        using the numeric representation of a spectrogram calculate the mean for each frequency bin

        :param Pxx:  a matrix that contains a numeric representation of the spectrogram figure
        :return: frequentcu_bin_arra
        '''

        frequentcy_bin_array = []
        for FragBin in Pxx:
            freq_bin_mean = np.average(FragBin)
            frequentcy_bin_array.append(freq_bin_mean)
        return frequentcy_bin_array

    def looper(self):
        '''
        loops through all the samples and apply's functions
        ends with outputting the results to a csv file
        :return:
        '''
        for sample in self.samplelist:
            Pxx = self.spectrogramdeterminer(sample)
            meaned_bins = self.meancalculator(Pxx)
            sample.bins = meaned_bins
            plt.clf()
            plt.close()
            self.queenlist.append(sample.queen)
        self.printer()

    def printer(self):
        '''
        writes a csv that contains round mean values for all the bins and labels [Queen, sample_name, Date, Time]

        label's explanation
            Queen: the label originating form the original filename the sample came form
            sample_name: a int that represents the sample number within a single wav file.
            Date: the date from the recording. originating for the filename
            Time: the time of recording. originating from the filename

        :return:
        '''
        dataframe = pd.DataFrame()
        for sample in self.samplelist:
            bin_values = [round(x, 5) for x in sample.bins]

            mfcc = pd.DataFrame(data=[bin_values], columns=["bin {}".format(x) for x in range(0, len(bin_values))])
            mfcc["Queen"] = sample.queen
            mfcc["Name"] = sample.name
            try:
                if len(sample.date) > 0:
                    mfcc["Date"] = sample.date
            except AttributeError:
                mfcc["Date"] = None
            try:
                if len(sample.time) > 0:
                    mfcc["Time"] = sample.time
            except AttributeError:
                mfcc["Time"] = None
            dataframe = dataframe.append(mfcc, ignore_index=True)

        dataframe.to_csv("{}/{}".format(self.output_path, 'queen_noqueen_binsize_15.csv'), index=None, header=True)
