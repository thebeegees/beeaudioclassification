import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
from Data.data import generate_data

print(tf.VERSION)
print(tf.keras.__version__)


# create random trainings data
#data = np.random.random((1000, 32))
#labels = np.random.random((1000, 10))

def labeler(labellist):
    for label in labellist:
        max_value = np.max(label)
        np.place(label, label != max_value, 0)
        np.place(label, label == max_value, 1)


#labeler(labels)

# create random validation data
#val_data = np.random.random((100, 32))
#val_labels = np.random.random((100, 10))

#labeler(val_labels  )

# create a model and add layers
model = tf.keras.Sequential()
# Adds a densely-connected layer with 64 units to the model:
model.add(layers.Dense(124, activation='relu'))
# Add another:
model.add(layers.Dense(64, activation='relu'))
# Add a softmax layer with 10 output units:
model.add(layers.Dense(10, activation='softmax'))

# compile the model
model.compile(optimizer=tf.train.AdamOptimizer(0.001),
              loss='categorical_crossentropy',
              metrics=['accuracy'])


data, labels = generate_data(4)
val_data, val_labels = generate_data(100)
print(data)
print([[x] for x in labels])
labels = [[x] for x in labels]
val_labels = [[x] for x in labels]
data = np.array(data)

labels = np.array(labels)

# training a model
model.fit(data, labels, epochs=500, batch_size=32)


# validating a model
hist = model.fit(data, labels, epochs=10, batch_size=32, validation_data=(val_data, val_labels))

print(hist.history)