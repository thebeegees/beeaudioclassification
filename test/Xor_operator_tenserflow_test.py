import tensorflow as tf
from tensorflow import keras
import numpy as np

# data try 1
xor_labels = [-1, 1, 1, -1]
xor_data = [[-1, -1], [-1, 1], [1, -1], [1, 1]]

xor_x1 = [-1, -1, 1,  1]
xor_x2 = [-1, 1, -1, 1]

# data try 2
xor_labels = np.hstack(xor_labels)
xor_data = np.hstack(xor_data)
xor_x1 = np.hstack(xor_x1)
xor_x2 = np.hstack(xor_x2)


# data try 3 tensor
xor_data_tensor = tf.Variable([[-1, -1], [-1, 1], [1, -1], [1, 1]], tf.float16)
xor_labels_tensor = tf.Variable([-1, 1, 1, -1], tf.float16)


bee_data = np.array([["bee", "bee"], ["bee", "queen"], ["queen", "bee"], ["queen", "queen"]])
bee_target = np.array([["bee"], ["queen"], ["queen"], ["bee"]])

# data try 4
training_data = np.array([[0, 0], [0, 1], [1, 0], [1, 1]], )
target = np.array([[0], [1], [1], [0]])


# model
xor_operator = tf.keras.Sequential()
xor_operator.add(keras.layers.Dense(32, input_dim=2, activation="relu"))
xor_operator.add(keras.layers.Dense(32, activation="relu"))
xor_operator.add(keras.layers.Dense(1, activation="sigmoid"))
xor_operator.compile(optimizer="adam",
                     loss="mean_squared_error",
                     metrics=['binary_accuracy'])

xor_operator.fit(training_data, target, epochs=500, verbose=2)

print(xor_operator.predict(training_data).round())
