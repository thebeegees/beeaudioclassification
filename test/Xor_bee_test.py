import numpy as np
from keras.models import Sequential
import tensorflow.keras.layers as layer

training_bee = ([['bee', 'bee'], ['queen', 'bee'], ['bee', 'bee']])
target_bee = ([['bee'], ['queen'], ['bee']])

model = Sequential()
model.add(layer.Dense(16, input_dims=2, activation='relu'))
model.add(layer.Dense(1, activation='sigmoid'))

model.compile(loss='mean_squared_error',
              optimizer='adam',
              metrics=['binary_accuracy'])

model.fit(training_bee, target_bee, nb_epoch=500, verbose=2)

print(model.predict(training_bee).round())